for (i = 0; i < RIP_form.length; i++) {// 遍历所有收到的RIP项目
    //查询router表中是否存在RIP报文相同目的TargetIP项目
    let FetchRouter = RouterForm.search(RIP_form[i].TargetIP);
    //router表中不存在RIP报文相同目的TargetIP的项目
    if (inRouter == null) {
        //将这个RIP报文记录在路由表
        RouterForm.add(RIP_form[i]);
    }
    //router表中存在RIP报文项目 且 目的IP相同
    else if (FetchRouter.TargetIP == RIP_Form[i].TargetIP) {
        //  整个更新(视为更新距离)
        FetchRouter = RIP_Form[i];
    }
    // 目的地址相同但是下一跳路由器不同，选择较小距离的路由
    else if (FetchRouter.NextJump > RIP_Form.NextJump) {
        // 更新NextJump
        FetchRouter.NextJump = RIP_Form[i].NextJump;
    } else {
        //啥都一样直接推出
        return 1;
    }
}

